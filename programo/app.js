const express = require('express')
const app = express()
const getRandomInt = require('./librerias/numeros');


//middleware global, genero un log con cada ejecución
app.use(function (req, res, next) {
    console.log('Time:', Date.now());
    next();
});


function particular(req, res, next) {
    console.log('Solo para algunos métodos');
    console.log('Time:', Date.now());
    next();
}

app.get('/particular', particular, function (req, res) {
    let mi_respuesta = {msj: 'Endpoint particular'}
    res.json(mi_respuesta)    
})



//dejo que continue al endpoint solamente cuando el numero es par
function middle_par(req, res, next) {
    const numero = getRandomInt(1, 100)
    let mi_respuesta = {numero: numero}
    if(numero % 2 == 0) {
        //envio en req.numero el numero al azar a la función del endpoint
        req.numero = numero
        next();
    }else{
        mi_respuesta.msj = "No es posible continuar, detengo la ejecución en el middleware por que NO EJECUTO next()"
        res.status(400).json(mi_respuesta)
    }
}

app.get('/pares', middle_par, function (req, res) {
    let mi_respuesta = {msj: 'El numero al azar es par'}
    mi_respuesta.numero = req.numero
    res.json(mi_respuesta)    
})


//parametros via get
//http://localhost:3000/es_par/7
app.get('/es_par/:id', function (req, res) {
    let numero = req.params.id
    let mi_respuesta = {numero: numero}
    if(numero % 2 == 0) {
        mi_respuesta.par = true;
        res.status(200)
    }else{
        mi_respuesta.par = false;
        res.status(400)
    }
    res.json(mi_respuesta)
});


//parametros via post
//http://localhost:3000/es_par/7
app.post('/es_par/:id', function (req, res) {
    let numero = req.params.id
    let mi_respuesta = {numero: numero}
    if(numero % 2 == 0) {
        mi_respuesta.par = true;
        res.status(200)
    }else{
        mi_respuesta.par = false;
        res.status(400)
    }
    res.json(mi_respuesta)
});

//middlewares para capturar los parametros enviados via post
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

//creo un array vehiculos para almacenar el "vehiculo" que voy a recibir como parametro
let vehiculos = []
app.post('/vehiculos', function (req, res, next) {
    vehiculos.push(req.body)
    res.json({msj: "El vehiculo ha sido insertado correctamente"})
})

app.get('/vehiculos', function (req, res, next) {
    res.json(vehiculos)
})









 
app.listen(3000, function() {
    console.log(`Express corriendo en http://localhost:3000`)
})