const compression = require('compression'); //npm i compression
const express = require('express');// npm i express
//instalar el nodemon npm i nodemon -D
const server = express(); //creacion del server


//hacer compresion del tamaño del response (de KB a Bytes) de todos los response al clientes
server.use(compression());


//configuracion de middlewares

/*function(req, res, next)
req> solicitudes
res> devoluciones al cliente
next> llamado a la siguiente funcion
*/

//tipos de middlewares
/*

De Aplicacion>
De ruta>
De manejo de errores>
De Terceros>
*/


//Middlewares de Aplicacion> Middlewares que son usados a un nivel global del servidor, son las primeras ejecutadas antes de llegar a cualquier ruta.

server.use(function(req, res, next) {
    console.log(req.url);
    //si no le coloca el express el endpoint nunca va a llegar a notificarse y no devolvera nada
    next();
});

// //middleware global, genero un log con cada ejecución
server.use(function (req, res, next) {
    console.log('Time:', Date.now());
    next();
});

// //Middlewares de Aplicacion>
function validarUsuario(req, res, next) {

    if(req.query.usuario != 'admin'){
        res.json("usuario invalido");
    }else{
        next();
    }
}

function interceptar(req, res, next) {
    res.json("Acceso denegado");    
}

//si lo asigno al server.use quedara como un middleware de forma global
//server.use(validarUsuario);
/******PETICIONES GET/POST/PUT......****************/

server.get('/', function(req, res){
    res.json("hola mundo");
});

server.get('/saludo_query/usuario',validarUsuario,function(req, res){

    let data = req.query;
    res.json("hola mundo, usuario validado correctamente");
});

server.get('/demo', interceptar,function(req, res){
    res.json("Hola mundo, interceptor");
});


server.get('/probarcompresion', function(req, res){
    const animal = 'alligator';
    // Send a text/html file back with the word 'alligator' repeated 1000 times
    res.send(animal.repeat(1000));
});




// server.get('/saludo', function(req, res){
//     res.json("hola mundo");
// });


// server.get('/saludo', function(req, res){
//     res.json("hola mundo");
// });

// //se asigna el middleware solo a la ruta para validar si el usuario o no es admin




server.listen(3000,function(){
    console.log('servidor ejecutandose en el puerto 3000');
});

